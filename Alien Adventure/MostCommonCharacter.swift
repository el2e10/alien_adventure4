//
//  MostCommonCharacter.swift
//  Alien Adventure
//
//  Created by Jarrod Parkes on 10/4/15.
//  Copyright © 2015 Udacity. All rights reserved.
//

extension Hero {
    
    func mostCommonCharacter(inventory: [UDItem]) -> Character? {
        var mostComDic = [Character:Int]()
        var count: Int = 0
        var mostComChar: Character?
        for item in inventory{
            for chara in item.name.lowercaseString.characters{
                if let charanum = mostComDic[chara]{
                    mostComDic[chara]! += 1
                }
                else{
                    mostComDic[chara] = 0
                }
            }
            
        }
        for (i,j) in mostComDic{
            if j > count{
                mostComChar = i
                count = j
            }
        }
        
        
        return mostComChar
    }
}
