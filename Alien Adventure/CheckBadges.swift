//
//  CheckBadges.swift
//  Alien Adventure
//
//  Created by Jarrod Parkes on 10/4/15.
//  Copyright © 2015 Udacity. All rights reserved.
//

extension Hero {
    
    func checkBadges(badges: [Badge], requestTypes: [UDRequestType]) -> Bool {
        
        
        for type in requestTypes {
            
            var matched: Bool = false
            
            for badge in badges {
                if badge.requestType == type {
                    matched = true
                    break
                }
            }
            if matched == false {
                return false
            }
        }
        
        return true
    }
    
    
}
