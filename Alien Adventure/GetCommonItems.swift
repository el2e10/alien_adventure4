//
//  GetCommonItems.swift
//  Alien Adventure
//
//  Created by Jarrod Parkes on 10/4/15.
//  Copyright © 2015 Udacity. All rights reserved.
//

extension Hero {
    
    func getCommonItems(inventory: [UDItem]) -> [UDItem] {
        
        
        
        let commonRarity = inventory.reduce([UDItem]() ) { (var itemarray, item) -> [UDItem] in
            if item.rarity.hashValue == UDItemRarity.Common.hashValue{
                itemarray.append(item)
            }
            return itemarray
        }
        
        
        
        return commonRarity
        
    }
    
}

// If you have completed this function and it is working correctly, feel free to skip this part of the adventure by opening the "Under the Hood" folder, and making the following change in Settings.swift: "static var RequestsToSkip = 6"