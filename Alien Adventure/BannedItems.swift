//
//  BannedItems.swift
//  Alien Adventure
//
//  Created by Jarrod Parkes on 10/4/15.
//  Copyright © 2015 Udacity. All rights reserved.
//

import Foundation

extension Hero {
    
    func bannedItems(dataFile: String) -> [Int] {
        
        var itemIDValues = [Int]()
        
        if let dataFileURL = NSBundle.mainBundle().URLForResource(dataFile, withExtension: "plist") {
            let data = NSArray(contentsOfURL: dataFileURL)
            
            
            
            for item in data! {
                var bannedName: String?
                var carbonAge: Int?
                
                if let item = item as? NSDictionary {
                    if let itemName = item["Name"] as? String {
                        bannedName = itemName
                        
                        if let historicalData = item["HistoricalData"] as? [String: AnyObject] {
                            if let itemCarbonAge = historicalData["CarbonAge"] as? Int {
                                carbonAge = itemCarbonAge
                            }
                        }
                        
                        if bannedName!.lowercaseString.containsString("laser") && carbonAge < 30 {
                            itemIDValues.append(item["ItemID"] as! Int)
                        }
                    }
                }
            }
        }
        
        return itemIDValues
        
        
    }
}

// If you have completed this function and it is working correctly, feel free to skip this part of the adventure by opening the "Under the Hood" folder, and making the following change in Settings.swift: "static var RequestsToSkip = 6"